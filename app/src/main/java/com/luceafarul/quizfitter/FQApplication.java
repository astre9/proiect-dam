package com.luceafarul.quizfitter;


import android.app.Application;
import android.util.Log;

import io.realm.Realm;
import io.realm.RealmConfiguration;

// Clasa pentru configurare a instantei de Realm la nivelul intregii aplicatii

public class FQApplication extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        try{
            Realm.init(getApplicationContext());
            RealmConfiguration config = new RealmConfiguration.Builder().name("user.realm").build();
            Realm.setDefaultConfiguration(config);
        }catch(Exception e){
            Log.d("Eroare", e.getMessage());
        }

    }
}
