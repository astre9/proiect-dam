package com.luceafarul.quizfitter.view;


import android.annotation.SuppressLint;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.icu.text.DateFormat;
import android.icu.text.SimpleDateFormat;
import android.os.Bundle;


import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Spinner;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.luceafarul.quizfitter.R;
import com.luceafarul.quizfitter.models.Exercise;
import com.luceafarul.quizfitter.models.Food;
import com.luceafarul.quizfitter.models.RoomUser;
import com.luceafarul.quizfitter.others.ExercisesAdapter;
import com.luceafarul.quizfitter.others.FoodAdapter;
import com.luceafarul.quizfitter.others.SharedPrefsFiles;
import com.luceafarul.quizfitter.repositories.room.GetFoodAsync;
import com.opencsv.CSVWriter;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class FoodListFragment extends Fragment {

    private ListView lvFood;
    private FoodAdapter foodAdapter;
    private Spinner spinner;
    private List<Food> foodList;
    FloatingActionButton fab;
    FloatingActionButton fabExport;

    private CSVWriter writer;
    static final DateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");

    public FoodListFragment() {
        // Required empty public constructor
    }


    @SuppressLint("StaticFieldLeak")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_food_list, container, false);
        lvFood = view.findViewById(R.id.lvFood);
        fab = view.findViewById(R.id.fab);
        fabExport = view.findViewById(R.id.fabExport);
        SharedPrefsFiles sharedPrefs = SharedPrefsFiles.getInstance(getContext());

        RoomUser loggedUser = new RoomUser();
        loggedUser.uid = Integer.parseInt(sharedPrefs.getString("id"));
        new GetFoodAsync(getContext()) {
            @Override
            protected void onPostExecute(List<Food> foods) {
                super.onPostExecute(foods);
                foodAdapter = new FoodAdapter(getContext(), foods);
                lvFood.setAdapter(foodAdapter);
                lvFood.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        int selectedFoodId = foodAdapter.getFoodList().get(position).fid;
                        sharedPrefs.saveString("selectedFoodId", String.valueOf(selectedFoodId));
                        changeFragment(new FoodFragment());
                    }
                });
            }
        }.execute(loggedUser);

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                changeFragment(new FoodFragment());
            }
        });

        fabExport.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    exportToCSV();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
        return view;
    }

    private void exportToCSV() throws IOException {
        String baseDir = getContext().getFilesDir().getAbsolutePath();
        Date date = new Date();
        String fileName = sdf.format(date) + "_food" + ".csv";
        String filePath = baseDir + File.separator + fileName;
        File f = new File(filePath);
        FileWriter fileWriter;
        // File exist
        if (f.exists() && !f.isDirectory()) {
            fileWriter = new FileWriter(filePath);
            writer = new CSVWriter(fileWriter);
        } else {
            writer = new CSVWriter(new FileWriter(filePath));
        }

        String[] data = {"Name", "Calories", "Protein", "Carbohydrate", "Fat"};
        writeLine(Arrays.toString(data));
        foodAdapter.getFoodList().forEach((food) -> writeLine(food.toString()));
        writer.close();
    }

    private void writeLine(String food) {
        String[] separated = food.split(",");
        writer.writeNext(separated);
    }

    private void changeFragment(Fragment fragment) {
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.replace(R.id.nav_host_fragment, fragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }
}
