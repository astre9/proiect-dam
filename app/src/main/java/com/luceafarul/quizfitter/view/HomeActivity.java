package com.luceafarul.quizfitter.view;

import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.luceafarul.quizfitter.R;
import com.luceafarul.quizfitter.models.RoomUser;
import com.luceafarul.quizfitter.models.User;
import com.google.android.material.navigation.NavigationView;
import com.luceafarul.quizfitter.others.SharedPrefsFiles;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;

public class HomeActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    private DrawerLayout drawerLayout;
    private TextView tvNavUser;
    private TextView tvMail;
    private ImageView ivProfile;
    private RoomUser user;
    private SharedPrefsFiles sharedPrefs;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        sharedPrefs = SharedPrefsFiles.getInstance(this);

        Intent intent = getIntent();
        user = intent.getParcelableExtra("User");

        configNavigation();
        changeFragment(new HomeFragment());

    }

    private void configNavigation() {
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        drawerLayout = findViewById(R.id.home_drawer_layout);
        ActionBarDrawerToggle actionBar =
                new ActionBarDrawerToggle(
                        this,
                        drawerLayout,
                        toolbar,
                        R.string.navigation_drawer_open,
                        R.string.navigation_drawer_close);
        drawerLayout.addDrawerListener(actionBar);
        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        View navHeader = navigationView.getHeaderView(0);
        TextView tvNavUser = navHeader.findViewById(R.id.tvNavUser);
        tvNavUser.setText("Welcome, " + user.getUsername());
        actionBar.syncState();
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case R.id.nav_home: {
                changeFragment(new HomeFragment());
                break;
            }
            case R.id.nav_profile: {
                changeFragment(new ProfileFragment());
                break;
            }
            case R.id.nav_food: {
                sharedPrefs.saveString("selectedFoodId","");
                changeFragment(new FoodListFragment());
                break;
            }
            case R.id.nav_play: {
                changeFragment(new WIPFragment());
                break;
            }
            case R.id.nav_gym: {
                changeFragment(new ExercisesFragment());
                break;
            }
            case R.id.nav_logout: {
                Logout();
            }
//            case R.id.nav_camera: {
//                changeFragment(new WIPFragment());
//                break;
//            }
//            case R.id.nav_bmi: {
//                changeFragment(new WIPFragment());
//                break;
//            }
        }
        drawerLayout.closeDrawer(GravityCompat.START);
        return true;
    }

    private void changeFragment(Fragment fragment) {
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.replace(R.id.nav_host_fragment, fragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }

    private void Logout() {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
        finish();
    }
}
