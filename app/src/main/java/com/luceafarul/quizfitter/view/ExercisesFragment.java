package com.luceafarul.quizfitter.view;

import android.os.Bundle;

import android.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Toast;

import com.luceafarul.quizfitter.R;
import com.luceafarul.quizfitter.models.Exercise;
import com.luceafarul.quizfitter.others.ExercisesAdapter;
import com.luceafarul.quizfitter.repositories.api.JSONRead;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * A simple {@link Fragment} subclass.
 */

//Fragmentul pentru afisare lista de exercitii

public class ExercisesFragment extends Fragment {

    private ListView lvExercises;
    private ExercisesAdapter exerciseAdapter;
    private Spinner spinner;
    private List<Exercise> exercises;

    public ExercisesFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_exercises, container, false);

        lvExercises = view.findViewById(R.id.lvExercises);
        spinner = view.findViewById(R.id.spDays);
        String[] days = new String[]{"push", "pull", "legs"};

        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(
                getActivity().getApplicationContext(), R.layout.spinner_item, days
        );
        spinnerArrayAdapter.setDropDownViewResource(R.layout.spinner_item);
        spinner.setAdapter(spinnerArrayAdapter);

        exerciseAdapter = new ExercisesAdapter(getActivity(), new ArrayList<Exercise>());
        lvExercises.setAdapter(exerciseAdapter);
        lvExercises.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Toast.makeText(getActivity(), exerciseAdapter.getExerciseList().get(position).toString(), Toast.LENGTH_LONG).show();
            }
        });
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                final String selectedItem = parent.getItemAtPosition(position).toString();
                if (exercises != null) {
                    List<Exercise> filteredExercises = exercises.stream()
                            .filter(p -> p.getDay().equals(selectedItem))
                            .collect(Collectors.toList());

                    exerciseAdapter.updateList(filteredExercises);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        connect();
        return view;
    }

    private void connect() {
        JSONRead jsonRead = new JSONRead() {
            @Override
            protected void onPostExecute(String s) {
                exercises = parseExerciseJson(s);
                exerciseAdapter.updateList(exercises);
            }
        };
        jsonRead.execute();
    }


}
