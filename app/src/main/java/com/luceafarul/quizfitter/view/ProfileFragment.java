package com.luceafarul.quizfitter.view;


import android.annotation.SuppressLint;
import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;


import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.QuickContactBadge;
import android.widget.Toast;

import com.luceafarul.quizfitter.R;
import com.luceafarul.quizfitter.models.RoomUser;
import com.luceafarul.quizfitter.others.SharedPrefsFiles;
import com.luceafarul.quizfitter.repositories.room.DeleteFoodAsync;
import com.luceafarul.quizfitter.repositories.room.DeleteUserAsync;
import com.luceafarul.quizfitter.repositories.room.GetUserAsync;
import com.luceafarul.quizfitter.repositories.room.UpdateFoodAsync;
import com.luceafarul.quizfitter.repositories.room.UpdateUserAsync;


/**
 * A simple {@link Fragment} subclass.
 */
public class ProfileFragment extends Fragment {

    EditText etEmail;
    EditText etUsername;
    EditText etPassword;
    Button btSave;
    Button btDelete;
    RoomUser loggedUser;
    SharedPrefsFiles sharedPrefs;

    public ProfileFragment() {
        // Required empty public constructor
    }


    @SuppressLint("StaticFieldLeak")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_profile, container, false);

        etEmail = view.findViewById(R.id.etMail);
        etPassword = view.findViewById(R.id.etNewPassword);
        etUsername = view.findViewById(R.id.etUsername);
        btSave = view.findViewById(R.id.btnSave);
        btDelete = view.findViewById(R.id.btnDelete);

        sharedPrefs = SharedPrefsFiles.getInstance(getContext());

        loggedUser = new RoomUser();
        loggedUser.username = sharedPrefs.getString("username");
        loggedUser.password = sharedPrefs.getString("password");

        new GetUserAsync(getContext()) {
            @Override
            protected void onPostExecute(RoomUser roomUser) {
                super.onPostExecute(roomUser);
                loggedUser = roomUser;
                etUsername.setText(loggedUser.username);
                etPassword.setText(loggedUser.password);
                etEmail.setText(loggedUser.email);
            }
        }.execute(loggedUser);

        btSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                loggedUser.username = etUsername.getText().toString();
                loggedUser.password= etPassword.getText().toString();
                loggedUser.email = etEmail.getText().toString();
                sharedPrefs.saveString("username", loggedUser.username);
                sharedPrefs.saveString("password", loggedUser.password);

                new UpdateUserAsync(getContext()).execute(loggedUser);
                Toast.makeText(getContext(), "Profile updated successfully!",Toast.LENGTH_LONG).show();
            }
        });

        btDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new DeleteUserAsync(getContext()){
                    @Override
                    protected void onPostExecute(Void aVoid) {
                        super.onPostExecute(aVoid);
                        Intent intent = new Intent(getActivity(), MainActivity.class);
                        getActivity().finish();
                        startActivity(intent);
                        Toast.makeText(getContext(),"Account deleted successfully!",Toast.LENGTH_LONG).show();
                    }
                }.execute(loggedUser);
            }
        });

        return view;
    }

}
