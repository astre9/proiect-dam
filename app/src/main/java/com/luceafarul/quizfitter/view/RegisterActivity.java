package com.luceafarul.quizfitter.view;

import android.content.Intent;
import android.os.Bundle;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Toast;

import com.luceafarul.quizfitter.R;
import com.luceafarul.quizfitter.models.RoomUser;
import com.luceafarul.quizfitter.presenters.RegisterPresenter;
import com.luceafarul.quizfitter.repositories.room.InsertFoodAsync;
import com.luceafarul.quizfitter.repositories.room.InsertUserAsync;

import androidx.appcompat.app.AppCompatActivity;

public class RegisterActivity extends AppCompatActivity implements RegisterPresenter.View {

    RegisterPresenter registerPresenter;

    Button btnRegister;
    EditText etUsername;
    EditText etPassword;
    EditText etCheckPassword;
    EditText etMail;
    CheckBox cbShowPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        registerPresenter = new RegisterPresenter(this);

        btnRegister = findViewById(R.id.btnRegister);
        etUsername = findViewById(R.id.etUsername);
        etPassword = findViewById(R.id.etNewPassword);
        etCheckPassword = findViewById(R.id.etPasswordCheck);
        etMail = findViewById(R.id.etEmail);
        cbShowPassword = findViewById(R.id.cbShowPassword);

        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String username = etUsername.getText().toString();
                String mail = etMail.getText().toString();
                String password = etPassword.getText().toString();

                if (validate()) {
                    registerPresenter.registerUser(username, password, mail, getBaseContext());
                } else {
                    Toast.makeText(getApplicationContext(), "Failed creating account!", Toast.LENGTH_LONG).show();
                }
            }
        });

        cbShowPassword.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (!isChecked) {
                    etPassword.setTransformationMethod(PasswordTransformationMethod.getInstance());
                } else {
                    etPassword.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                }
            }
        });

    }

    // metoda de validare pentru creare cont nou
    public boolean validate() {
        boolean result = true;
        if (etUsername.getText().toString().equals("") || etUsername.getText() == null || etUsername.getText().length() < 6) {
            etUsername.setError("Username must have more than 6 characters!");
            result = false;
        } else {
            etUsername.setError(null);
        }

        if (etPassword.getText().toString().equals("") || etPassword.getText() == null || etPassword.getText().length() < 6) {
            etPassword.setError("Password must have more than 6 characters!");
            result = false;
        } else {
            etPassword.setError(null);
        }

        if (etMail.getText().toString().equals("") || etMail.getText() == null || etMail.getText().length() < 6) {
            etMail.setError("E-mail must have more than 6 characters!");
            result = false;
        } else {
            etUsername.setError(null);
        }

        if (etCheckPassword.getText().toString().equals("") || etCheckPassword.getText() == null || etCheckPassword.getText().length() < 6 || !etCheckPassword.getText().toString().equals(etPassword.getText().toString())) {
            etCheckPassword.setError("Password doesn't match!");
            result = false;
        } else {
            etCheckPassword.setError(null);
        }
        return result;
    }

    // metoda de modificare a UI in urma incercarii de creeare a unui nou cont
    @Override
    public void notifyRegister(String mesaj) {
        Toast.makeText(this, mesaj, Toast.LENGTH_LONG).show();
        if (mesaj.equals("Account created successfully!")) {
            String username = etUsername.getText().toString();
            String password = etPassword.getText().toString();


            Intent loginIntent = new Intent(this, MainActivity.class);
            loginIntent.putExtra("username", username);
            loginIntent.putExtra("password", password);
            loginIntent.putExtra("email", etMail.getText().toString());
            finish();
            startActivity(loginIntent);
        }
    }
}
