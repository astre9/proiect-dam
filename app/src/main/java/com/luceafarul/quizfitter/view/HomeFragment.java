package com.luceafarul.quizfitter.view;


import android.app.Fragment;
import android.app.FragmentTransaction;
import android.media.Image;
import android.os.Bundle;


import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.luceafarul.quizfitter.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class HomeFragment extends Fragment {

    private ImageView ivPlay;
    private ImageView ivGym;
    private ImageView ivFood;

    public HomeFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View view = inflater.inflate(R.layout.fragment_home, container, false);
        ivPlay = view.findViewById(R.id.ivPlay);
        ivFood = view.findViewById(R.id.ivFoodWP);
        ivGym = view.findViewById(R.id.ivGymWP);

        ivPlay.setOnClickListener(v -> changeFragment(new WIPFragment()));
        ivGym.setOnClickListener(v -> changeFragment(new ExercisesFragment()));
        ivFood.setOnClickListener(v -> changeFragment(new WIPFragment()));

        return view;
    }
    private void changeFragment(Fragment fragment) {
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.replace(R.id.nav_host_fragment, fragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }

}
