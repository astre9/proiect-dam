package com.luceafarul.quizfitter.view;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.luceafarul.quizfitter.R;
import com.luceafarul.quizfitter.models.RoomUser;
import com.luceafarul.quizfitter.models.User;
import com.luceafarul.quizfitter.others.SharedPrefsFiles;
import com.luceafarul.quizfitter.presenters.LoginPresenter;
import com.google.firebase.database.DatabaseReference;

import androidx.appcompat.app.AppCompatActivity;

// Activitate de login

public class MainActivity extends AppCompatActivity implements LoginPresenter.View {

    private Button btnLogin;
    private Button btnRegister;
    private EditText etUsername;
    private EditText etPassword;
    private LoginPresenter loginPresenter;
    private Context that;
    private SharedPrefsFiles prefs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        String newUsername = null;
        String newPassword = null;

        try {
            newUsername = getIntent().getExtras().getString("username");
            newPassword = getIntent().getExtras().getString("password");
        } catch (Exception e) {
            e.printStackTrace();
        }

        that = this;
        loginPresenter = new LoginPresenter(this);

        btnLogin = findViewById(R.id.btnLogin);
        btnRegister = findViewById(R.id.btnRegister);
        etPassword = findViewById(R.id.etParola);
        etUsername = findViewById(R.id.etUsername);

        prefs = SharedPrefsFiles.getInstance(this);

        if (newUsername != null && newPassword != null) {
            etUsername.setText(newUsername);
            etPassword.setText(newPassword);
        }

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                RoomUser user = new RoomUser();
                user.username = etUsername.getText().toString();
                user.password = etPassword.getText().toString();
                loginPresenter.loginUser(user, getBaseContext());
            }
        });

        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(that, RegisterActivity.class);
                startActivity(intent);
            }
        });
    }

    // metoda ce modifica UI in urma login-ului
    @Override
    public void notifyLogin(String mesaj, RoomUser user) {
        Toast.makeText(that, mesaj, Toast.LENGTH_SHORT).show();
        if (mesaj.equals("Login successful!")) {
            prefs.saveString("id", String.valueOf(user.getUid()));
            prefs.saveString("username", user.getUsername());
            prefs.saveString("password", user.getPassword());
            Intent homeIntent = new Intent(this.getApplicationContext(), HomeActivity.class);
            homeIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            homeIntent.putExtra("User", user);
            startActivity(homeIntent);
            finish();
        }
    }
}
