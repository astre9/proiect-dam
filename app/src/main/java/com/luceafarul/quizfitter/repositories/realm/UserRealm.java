package com.luceafarul.quizfitter.repositories.realm;

import com.luceafarul.quizfitter.models.User;

import java.util.Random;
import java.util.UUID;

import io.realm.Realm;
import io.realm.RealmResults;
import io.realm.exceptions.RealmException;

// Clasa unde sunt implementate metodele din interfata RealmOperations

public class UserRealm implements RealmOperations {

    private Realm realm;

    public UserRealm() {
        try {
            realm = Realm.getDefaultInstance();
        } catch (RealmException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void insert(User model) {
        realm.beginTransaction();
        Random rng = new Random();
        model.setId(UUID.randomUUID().toString());
        User newUser = realm.copyToRealm(model);
        realm.commitTransaction();
    }

    @Override
    public String exists(Object model) {
        RealmResults<User> results = realm.where(User.class)
                .equalTo("username", ((User) model).getUsername())
                .and()
                .equalTo("password", ((User) model).getPassword())
                .findAll();
        if (results.isEmpty())
            return null;
        else return results.first().getId();
    }

    @Override
    public void close() {
        realm.close();
    }
}
