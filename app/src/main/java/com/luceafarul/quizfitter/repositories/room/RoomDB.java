package com.luceafarul.quizfitter.repositories.room;

import com.luceafarul.quizfitter.models.Food;
import com.luceafarul.quizfitter.models.RoomUser;
import com.luceafarul.quizfitter.models.User;

import androidx.room.Database;
import androidx.room.RoomDatabase;

@Database(entities = {Food.class, RoomUser.class}, version = 1, exportSchema = false)
public abstract class RoomDB extends RoomDatabase {

    public abstract FoodDao foodsDAO();
    public abstract UserDao usersDao();
}
