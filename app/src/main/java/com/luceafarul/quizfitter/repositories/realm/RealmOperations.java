package com.luceafarul.quizfitter.repositories.realm;

import com.luceafarul.quizfitter.models.User;

// Interfata pentru operatiuni pe baza de date locala

public interface RealmOperations {
    void insert(User model);

    String exists(Object model);

    void close();
}
