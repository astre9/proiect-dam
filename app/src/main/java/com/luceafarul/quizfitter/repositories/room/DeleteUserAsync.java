package com.luceafarul.quizfitter.repositories.room;

import android.content.Context;
import android.os.AsyncTask;

import androidx.room.Room;

import com.luceafarul.quizfitter.models.Food;
import com.luceafarul.quizfitter.models.RoomUser;

public class DeleteUserAsync extends AsyncTask<RoomUser, Void, Void> {
    private Context context;

    public DeleteUserAsync(Context context) {
        this.context = context;
    }

    @Override
    protected Void doInBackground(RoomUser... users) {
        DataBase.getInstance(context).getDatabase().usersDao().delete(users[0]);
        return null;
    }
}
