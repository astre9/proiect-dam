package com.luceafarul.quizfitter.presenters;

import android.annotation.SuppressLint;
import android.content.Context;

import com.luceafarul.quizfitter.models.RoomUser;
import com.luceafarul.quizfitter.models.User;
import com.luceafarul.quizfitter.repositories.realm.UserRealm;
import com.luceafarul.quizfitter.repositories.room.GetUserAsync;
import com.luceafarul.quizfitter.repositories.room.InsertUserAsync;

import org.apache.commons.collections4.Get;

import javax.annotation.Nullable;

// Presenter pentru login activity

public class LoginPresenter {
    private View view;

//    private UserRealm realm;

    public LoginPresenter(View view) {
        this.view = view;
//        this.realm = new UserRealm();
    }

    @SuppressLint("StaticFieldLeak")
    public void loginUser(RoomUser user, Context context) {

        new GetUserAsync(context) {
            @Override
            protected void onPostExecute(RoomUser roomUser) {
                super.onPostExecute(roomUser);
                boolean existsUser = true;
                if (roomUser == null)
                    existsUser = false;
                if (existsUser) {
                    user.uid = roomUser.uid;
                    view.notifyLogin("Login successful!", user);
                } else {
                    view.notifyLogin("Login failed!", null);
                }
            }
        }.execute(user);
//        String foundId = realm.exists(user);
//        if (foundId != null) {
//            user.setId(foundId);
//            view.notifyLogin("Logare cu succes!", user);
//            return true;
//        } else {
//            view.notifyLogin("Logare esuata!", null);
//            return false;
//        }
    }

//    public void closeRealm() {
//        if (realm != null)
//            realm.close();
//    }

    public interface View {
        void notifyLogin(String mesaj, RoomUser user);
    }
}
