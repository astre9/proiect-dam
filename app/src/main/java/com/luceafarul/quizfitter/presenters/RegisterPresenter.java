package com.luceafarul.quizfitter.presenters;

import android.annotation.SuppressLint;
import android.content.Context;

import com.luceafarul.quizfitter.models.RoomUser;
import com.luceafarul.quizfitter.models.User;
import com.luceafarul.quizfitter.repositories.realm.UserRealm;
import com.luceafarul.quizfitter.repositories.room.DataBase;
import com.luceafarul.quizfitter.repositories.room.GetUserAsync;
import com.luceafarul.quizfitter.repositories.room.InsertUserAsync;
import com.luceafarul.quizfitter.view.RegisterActivity;

import java.util.Random;

// Presenter pentru Register activity

public class RegisterPresenter {
    private View view;
    boolean existsUser = true;
    DataBase dataBase;

    public RegisterPresenter(View view) {
        this.view = view;
    }

    public void registerUser(String username, String password,String email, Context context) {

        RoomUser roomUser = new RoomUser();
        roomUser.username = username;
        roomUser.password = password;
        roomUser.email = email;
        dataBase.getInstance(context);

        new GetUserAsync(context) {
            @Override
            protected void onPostExecute(RoomUser roomUser) {
                super.onPostExecute(roomUser);
                if (roomUser == null) {
                    existsUser = false;
                }
                if (!existsUser) {
                    roomUser = new RoomUser();
                    roomUser.username = username;
                    roomUser.password = password;
                    roomUser.email = email;
                    roomUser.uid = new Random().nextInt();
                    new InsertUserAsync(context).execute(roomUser);
                    view.notifyRegister("Account created successfully!");
                } else {
                    view.notifyRegister("Another account already exists!");
                }
            }
        }.execute(roomUser);


        //        UserRealm realm = new UserRealm();
        //        User user = new User(username = username, password = password, email = email);
        //        if (realm.exists(user) == null) {
        //            realm.insert(user);
        //            view.notifyRegister("Contul a fost creat cu succes!");
        //        } else {
        //            view.notifyRegister("Exista deja un cont cu datele introduse!");
        //        }
        //        realm.close();

    }

    public interface View {
        void notifyRegister(String mesaj);
    }
}
